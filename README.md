Base Project for TDD Workshop
=============================

![Build Status](https://gitlab.com/workshop-tdd/ruby-quickstart/badges/master/build.svg)

This is a simple example that shows the integration of several tools: sinatra + cucumber + rspec + rake